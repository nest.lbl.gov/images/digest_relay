#!/bin/bash
#
# Processes *.txt.tmpl file to resolved variables

process_txt_tmpl() {
    output=${1%.tmpl}
    if [ "X${1}" != "X${output}" -a -f "${1}" ] ; then
        echo "Creating \"${output}\""
        eval "echo \"$(sed 's/"/\\"/g' ${1})\"" > ${output}
    fi
}

if [ 0 == $# ] ; then
    for tmpl in $(find . -name "*.txt.tmpl") ; do
        process_txt_tmpl ${tmpl}
    done
else
    for tmpl in "${@}" ; do
        process_txt_tmpl ${tmpl}
    done
fi
