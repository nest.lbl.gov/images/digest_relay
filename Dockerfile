FROM python:3.10-alpine
LABEL org.opencontainers.image.authors="<Simon Patton> sjpatton@lbl.gov, <Keith Beattie> ksbeattie@lbl.gov"

ENV PYTHONUNBUFFERED=1 \
    PIP_ROOT_USER_ACTION=ignore

COPY requirements.txt requirements.txt
# Set up Alpine to build `psutil`
RUN apk add bash coreutils build-base python3-dev linux-headers \
    && pip install -r requirements.txt --upgrade pip

CMD digest_relay
